package xin.examinationSystem.imp;

import com.alibaba.fastjson.JSON;
import org.junit.jupiter.api.Test;
import xin.examinationSystem.account.Student;
import xin.examinationSystem.account.testPaper.StuLookTestPaperSet;
import xin.examinationSystem.account.testPaper.TestPaperSet;
import xin.examinationSystem.dao.IAdminMapper;
import xin.examinationSystem.dao.IStudentMapper;
import xin.examinationSystem.dao.topic.ITestPaperMapper;
import xin.examinationSystem.utils.Tools;

import java.util.Arrays;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TestPaperMapperImpTest {
    @Test
    void fun() {
        ITestPaperMapper iTestPaperMapper = (ITestPaperMapper) Tools.getImp("ITestPaperMapper");
        StuLookTestPaperSet[] StuLookTestPaperSets = iTestPaperMapper.query_stu_all_tpSet(82);
        Arrays.stream(StuLookTestPaperSets).forEach(System.out::println);
    }
}