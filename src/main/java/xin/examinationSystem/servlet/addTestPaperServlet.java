package xin.examinationSystem.servlet;

import com.alibaba.fastjson.JSON;
import xin.examinationSystem.account.Teacher;
import xin.examinationSystem.account.testPaper.topic.*;
import xin.examinationSystem.dao.ITeacherMapper;
import xin.examinationSystem.dao.topic.ITestPaperMapper;
import xin.examinationSystem.utils.Tools;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/testPaperServlet")
public class addTestPaperServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean logInStatus = false;
        //判断是否要自动登录
        String str = request.getParameter("teaAutoLogIn");
        //如果客户端存在cookie
        if (str != null && str.equals("YES")) {
            Cookie[] cookies = request.getCookies();
            for (Cookie tCookie : cookies) {
                if (tCookie.getName().equals("accountMessageTeacher")) {
                    HttpSession session = request.getSession();
                    Teacher attribute = (Teacher) session.getAttribute(tCookie.getValue());
                    if (attribute != null) {
                        logInStatus = true;
                    }
                }
            }
        }
        //如果没有登录,重定向到登录页面
        if (logInStatus) {
            response.sendRedirect("/index.html");
            return;
        }

        ITestPaperMapper iTestPaperMapper = (ITestPaperMapper) Tools.getImp("ITestPaperMapper");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        //添加单选题请求
        String add_single_choice = request.getParameter("add_single_choice");
        if (add_single_choice != null) {
            SingleChoice singleChoice = JSON.parseObject(add_single_choice, SingleChoice.class);
            iTestPaperMapper.addSingleChoice(singleChoice);
            out.write("succeed");
            out.flush();
            out.close();
            return;
        }

        //添加单选题请求
        String add_multiple_choice_question = request.getParameter("add_multiple_choice_question");
        if (add_multiple_choice_question != null) {
            System.out.println("add_multiple_choice_question: " + add_multiple_choice_question);
            MultipleChoiceQuestion multipleChoiceQuestion = JSON.parseObject(add_multiple_choice_question, MultipleChoiceQuestion.class);
            iTestPaperMapper.addMultipleChoiceQuestion(multipleChoiceQuestion);
            out.write("succeed");
            out.flush();
            out.close();
            return;
        }

        //添加单选题请求
        String add_judge_question = request.getParameter("add_judge_question");
        if (add_judge_question != null) {
            JudgeQuestion judgeQuestion = JSON.parseObject(add_judge_question, JudgeQuestion.class);
            iTestPaperMapper.addJudgeQuestion(judgeQuestion);
            out.write("succeed");
            out.flush();
            out.close();
            return;
        }

        //添加单选题请求
        String add_gap_filling = request.getParameter("add_gap_filling");
        if (add_gap_filling != null) {
            GapFilling gapFilling = JSON.parseObject(add_gap_filling, GapFilling.class);
            iTestPaperMapper.addGapFilling(gapFilling);
            out.write("succeed");
            out.flush();
            out.close();
            return;
        }

        //添加单选题请求
        String add_essay_question = request.getParameter("add_essay_question");
        if (add_essay_question != null) {
            EssayQuestion essayQuestion = JSON.parseObject(add_essay_question, EssayQuestion.class);
            iTestPaperMapper.addEssayQuestion(essayQuestion);
            out.write("succeed");
            out.flush();
            out.close();
            return;
        }



        out.write("fail");
        out.flush();
        out.close();
    }
}
