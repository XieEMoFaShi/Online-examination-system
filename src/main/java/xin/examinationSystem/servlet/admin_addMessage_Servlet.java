package xin.examinationSystem.servlet;

import com.alibaba.fastjson.JSON;
import xin.examinationSystem.account.Admin;
import xin.examinationSystem.account.Student;
import xin.examinationSystem.account.Teacher;
import xin.examinationSystem.dao.IAdminMapper;
import xin.examinationSystem.dao.IStudentMapper;
import xin.examinationSystem.dao.ITeacherMapper;
import xin.examinationSystem.utils.Tools;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

@WebServlet("/admin_addMessage_Servlet")
public class admin_addMessage_Servlet extends HttpServlet {
    public void init() {

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        boolean adminStatus = false;
        boolean teacherStatus = false;
        Cookie[] cookies = request.getCookies();
        for (Cookie tCookie : cookies) {
            if (tCookie.getName().equals("accountMessage")) {
                HttpSession session = request.getSession();
                Admin attribute = (Admin) session.getAttribute(tCookie.getValue());
                if (attribute != null) {
                    adminStatus = true;
                }
            }
        }

        for (Cookie tCookie : cookies) {
            if (tCookie.getName().equals("accountMessageTeacher")) {
                HttpSession session = request.getSession();
                Teacher attribute = (Teacher) session.getAttribute(tCookie.getValue());
                if (attribute != null) {
                    teacherStatus = true;
                }
            }
        }


        if (!adminStatus && !teacherStatus) {
            response.sendRedirect("/index.html");
            return;
        }

        //判断是否添加的是学生信息--------------------------------------------------------------------------------
        String studentMessage = request.getParameter("addStudentMessage");
        if (studentMessage != null) {
            //把JSON转为对象,再把对象保存到数据库
            Student student = JSON.parseObject(studentMessage, Student.class);
            IAdminMapper iAdminMapper = (IAdminMapper) Tools.getImp("IAdminMapper");
            IStudentMapper iStudentMapper = (IStudentMapper) Tools.getImp("IStudentMapper");
            Student stu = iStudentMapper.findAccount(student.getStuAccount());

            //如果账号已经存在
            if (stu != null) {
                System.out.println("学生账号已存在");
                response.setContentType("text/html;charset=UTF-8");
                PrintWriter out = response.getWriter();
                out.write("NO");
                out.flush();
                out.close();
                return;
            }

            iAdminMapper.addStudent(student);
            System.out.println("添加学生信息成功!");
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write("OK");
            out.flush();
            out.close();
            return;
        }

        //判断是否添加的是教师信息----------------------------------------------------------------------------
        String teacherMessage = request.getParameter("addTeacherMessage");
        if (teacherMessage != null) {
            //把JSON转为对象,再把对象保存到数据库
            Teacher teacher = JSON.parseObject(teacherMessage, Teacher.class);
            IAdminMapper iAdminMapper = (IAdminMapper) Tools.getImp("IAdminMapper");
            ITeacherMapper iTeacherMapper = (ITeacherMapper) Tools.getImp("ITeacherMapper");

            //判断是否存在该账号
            Teacher teaId = iTeacherMapper.findAccount(teacher.getTeaAccount());
            if (teaId != null) {
                response.setContentType("text/html;charset=UTF-8");
                PrintWriter out = response.getWriter();
                out.write("NO");
                out.flush();
                out.close();
                return;
            }
            iAdminMapper.addTeacher(teacher);
            System.out.println("添加教师信息成功!");
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write("OK");
            out.flush();
            out.close();
            return;
        }
    }

    public void destroy() {
    }
}