package xin.examinationSystem.servlet;

import xin.examinationSystem.account.Admin;
import xin.examinationSystem.account.Student;
import xin.examinationSystem.account.Teacher;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 用于判断是否登录,没有登录就重定向到登录页面,没有完成
 */
@WebServlet("/exitLogin")
public class exitLogin extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean logInStatus = false;

        Cookie[] cookies = request.getCookies();
        if (request.getParameter("type").equals("teacher")) {
            for (Cookie tCookie : cookies) {
                if (tCookie.getName().equals("accountMessageTeacher")) {
                    HttpSession session = request.getSession();
                    session.removeAttribute(tCookie.getValue());
                    logInStatus = true;
                }
            }
        }
        if (request.getParameter("type").equals("student")) {
            for (Cookie tCookie : cookies) {
                if (tCookie.getName().equals("accountMessageStudent")) {
                    HttpSession session = request.getSession();
                    session.removeAttribute(tCookie.getValue());
                    logInStatus = true;
                }
            }
        }

        if (request.getParameter("type").equals("admin")) {
            for (Cookie tCookie : cookies) {
                if (tCookie.getName().equals("accountMessage")) {
                    HttpSession session = request.getSession();
                    session.removeAttribute(tCookie.getValue());
                    logInStatus = true;
                }
            }
        }


        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        //如果没有登录,重定向到登录页面
        if (logInStatus) {
            out.write("OK");
        }
        out.flush();
        out.close();
    }
}
