package xin.examinationSystem.account.testPaper;

import xin.examinationSystem.account.testPaper.topic.*;

import java.util.Arrays;

public class TopicSet {
    private SingleChoice[] singleChoices;
    private MultipleChoiceQuestion[] multipleChoiceQuestions;
    private JudgeQuestion[] judgeQuestions;
    private GapFilling[] gapFillings;
    private EssayQuestion[] essayQuestions;

    public SingleChoice[] getSingleChoices() {
        return singleChoices;
    }

    public void setSingleChoices(SingleChoice[] singleChoices) {
        this.singleChoices = singleChoices;
    }

    public MultipleChoiceQuestion[] getMultipleChoiceQuestions() {
        return multipleChoiceQuestions;
    }

    public void setMultipleChoiceQuestions(MultipleChoiceQuestion[] multipleChoiceQuestions) {
        this.multipleChoiceQuestions = multipleChoiceQuestions;
    }

    public JudgeQuestion[] getJudgeQuestions() {
        return judgeQuestions;
    }

    public void setJudgeQuestions(JudgeQuestion[] judgeQuestions) {
        this.judgeQuestions = judgeQuestions;
    }

    public GapFilling[] getGapFillings() {
        return gapFillings;
    }

    public void setGapFillings(GapFilling[] gapFillings) {
        this.gapFillings = gapFillings;
    }

    public EssayQuestion[] getEssayQuestions() {
        return essayQuestions;
    }

    public void setEssayQuestions(EssayQuestion[] essayQuestions) {
        this.essayQuestions = essayQuestions;
    }

    @Override
    public String toString() {
        return "TopicSet{" +
                "singleChoices=" + Arrays.toString(singleChoices) +
                ", multipleChoiceQuestions=" + Arrays.toString(multipleChoiceQuestions) +
                ", judgeQuestions=" + Arrays.toString(judgeQuestions) +
                ", gapFillings=" + Arrays.toString(gapFillings) +
                ", essayQuestions=" + Arrays.toString(essayQuestions) +
                '}';
    }
}
