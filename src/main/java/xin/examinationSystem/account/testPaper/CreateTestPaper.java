package xin.examinationSystem.account.testPaper;

import xin.examinationSystem.account.testPaper.topic.*;

import java.util.Arrays;

public class CreateTestPaper {
    private SingleChoice[] singleChoices;
    private MultipleChoiceQuestion[] multipleChoiceQuestion;
    private JudgeQuestion[] judgeQuestion;
    private GapFilling[] gapFilling;
    private EssayQuestion[] essayQuestion;

    public SingleChoice[] getSingleChoices() {
        return singleChoices;
    }

    public void setSingleChoices(SingleChoice[] singleChoices) {
        this.singleChoices = singleChoices;
    }

    public MultipleChoiceQuestion[] getMultipleChoiceQuestion() {
        return multipleChoiceQuestion;
    }

    public void setMultipleChoiceQuestion(MultipleChoiceQuestion[] multipleChoiceQuestion) {
        this.multipleChoiceQuestion = multipleChoiceQuestion;
    }

    public JudgeQuestion[] getJudgeQuestion() {
        return judgeQuestion;
    }

    public void setJudgeQuestion(JudgeQuestion[] judgeQuestion) {
        this.judgeQuestion = judgeQuestion;
    }

    public GapFilling[] getGapFilling() {
        return gapFilling;
    }

    public void setGapFilling(GapFilling[] gapFilling) {
        this.gapFilling = gapFilling;
    }

    public EssayQuestion[] getEssayQuestion() {
        return essayQuestion;
    }

    public void setEssayQuestion(EssayQuestion[] essayQuestion) {
        this.essayQuestion = essayQuestion;
    }

    @Override
    public String toString() {
        return "createTestPaper{" +
                "singleChoices=" + Arrays.toString(singleChoices) +
                ", multipleChoiceQuestion=" + Arrays.toString(multipleChoiceQuestion) +
                ", judgeQuestion=" + Arrays.toString(judgeQuestion) +
                ", gapFilling=" + Arrays.toString(gapFilling) +
                ", essayQuestion=" + Arrays.toString(essayQuestion) +
                '}';
    }
}
