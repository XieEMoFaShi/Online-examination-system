package xin.examinationSystem.account.testPaper.topic;

/**
 * 填空题类
 */
public class GapFilling {
    /**
     *题目编号
     */
    private Integer id;

    /**
     *题目内容
     */
    private String topic;

    /**
     *题目答案
     */
    private String answer;

    /**
     * 填空题，需要填的数量
     */
    private Integer number;

    public GapFilling() {
    }

    public GapFilling(String topic) {
        this.topic = topic;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }


    @Override
    public String toString() {
        return "GapFilling{" +
                "id=" + id +
                ", topic='" + topic + '\'' +
                ", answer='" + answer + '\'' +
                ", number=" + number +
                '}';
    }
}
