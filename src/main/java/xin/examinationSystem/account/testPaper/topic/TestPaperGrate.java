package xin.examinationSystem.account.testPaper.topic;

public class TestPaperGrate {

    private Integer tpId;
    private Integer scGrate;
    private Integer msqGrate;
    private Integer jqGrate;
    private Integer gfGrate;
    private Integer eqGrate;

    public TestPaperGrate() {
    }

    public TestPaperGrate(Integer tpId, Integer scGrate, Integer msqGrate, Integer jqGrate, Integer gfGrate, Integer eqGrate) {
        this.tpId = tpId;
        this.scGrate = scGrate;
        this.msqGrate = msqGrate;
        this.jqGrate = jqGrate;
        this.gfGrate = gfGrate;
        this.eqGrate = eqGrate;
    }

    public Integer getTpId() {
        return tpId;
    }

    public void setTpId(Integer tpId) {
        this.tpId = tpId;
    }

    public Integer getScGrate() {
        return scGrate;
    }

    public void setScGrate(Integer scGrate) {
        this.scGrate = scGrate;
    }

    public Integer getMsqGrate() {
        return msqGrate;
    }

    public void setMsqGrate(Integer msqGrate) {
        this.msqGrate = msqGrate;
    }

    public Integer getJqGrate() {
        return jqGrate;
    }

    public void setJqGrate(Integer jqGrate) {
        this.jqGrate = jqGrate;
    }

    public Integer getGfGrate() {
        return gfGrate;
    }

    public void setGfGrate(Integer gfGrate) {
        this.gfGrate = gfGrate;
    }

    public Integer getEqGrate() {
        return eqGrate;
    }

    public void setEqGrate(Integer eqGrate) {
        this.eqGrate = eqGrate;
    }

    @Override
    public String toString() {
        return "TestPaperGrate{" +
                "tpId=" + tpId +
                ", scGrate=" + scGrate +
                ", msqGrate=" + msqGrate +
                ", jqGrate=" + jqGrate +
                ", gfGrate=" + gfGrate +
                ", eqGrate=" + eqGrate +
                '}';
    }
}
