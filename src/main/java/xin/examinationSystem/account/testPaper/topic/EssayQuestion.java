package xin.examinationSystem.account.testPaper.topic;

public class EssayQuestion {
    /**
     * 题目编号
     */
    private Integer id;

    /**
     *题目内容
     */
    private String topic;

    /**
     *题目答案
     */
    private String answer;

    public EssayQuestion() {
    }

    public EssayQuestion(String topic) {
        this.topic = topic;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "EssayQuestion{" +
                "id=" + id +
                ", topic='" + topic + '\'' +
                ", answer='" + answer + '\'' +
                '}';
    }
}
