package xin.examinationSystem.account.testPaper.topic;

/**
 *判断题类
 */
public class JudgeQuestion {
    /**
     *题目编号
     */
    private Integer id;

    /**
     *题目内容
     */
    private String topic;

    /**
     *题目答案
     */
    private String answer;

    public JudgeQuestion() {
    }

    public JudgeQuestion(String topic, String answer) {
        this.topic = topic;
        this.answer = answer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "judgeQuestion{" +
                "id=" + id +
                ", topic='" + topic + '\'' +
                ", answer='" + answer + '\'' +
                '}';
    }
}
