package xin.examinationSystem.account.testPaper.topic;

public class Student_test_paper {
    private Integer stuId;
    private Integer tpId;
    private String node = "";
    private String answer = "";
    private Integer grade = -1;

    public Integer getStuId() {
        return stuId;
    }

    public void setStuId(Integer stuId) {
        this.stuId = stuId;
    }

    public Integer getTpId() {
        return tpId;
    }

    public void setTpId(Integer tpId) {
        this.tpId = tpId;
    }

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "student_test_paper{" +
                "stuId=" + stuId +
                ", tpId=" + tpId +
                ", node='" + node + '\'' +
                ", answer='" + answer + '\'' +
                ", grade=" + grade +
                '}';
    }
}
