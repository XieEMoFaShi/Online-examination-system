package xin.examinationSystem.account.testPaper.excelTopic;

import com.alibaba.excel.annotation.ExcelIgnore;
import lombok.Data;

/**
 * 所有Excel的实体类都要实现该接口
 */
@Data
public class ExcelClass {
    @ExcelIgnore
    TopicId topicId = TopicId.SingleChoiceExcel;
}
