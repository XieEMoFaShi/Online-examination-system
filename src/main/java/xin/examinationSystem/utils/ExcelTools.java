package xin.examinationSystem.utils;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import xin.examinationSystem.account.testPaper.excelTopic.TopicId;

import java.io.FileInputStream;
import java.io.IOException;

public class ExcelTools {

    static public boolean judgeFormatIfCorrect(TopicId topicId, String filePath) throws IOException {
        if (!filePath.endsWith(".xlsx")) {
            return false;
        }
        FileInputStream fileInputStream = new FileInputStream(filePath);

        Workbook workbook = new XSSFWorkbook(fileInputStream);

        //得到工作表,第一页
        Sheet sheet = workbook.getSheetAt(0);

        if (topicId == TopicId.SingleChoiceExcel) {
            Row row = sheet.getRow(0);
            //getCell获取单元格,getStringCellValue获取单元格中的内容
            if (!row.getCell(0).getStringCellValue().equals("题目") || !row.getCell(1).getStringCellValue().equals("正确选项(1,2,3,4)")
                    || !row.getCell(2).getStringCellValue().equals("选项A") || !row.getCell(3).getStringCellValue().equals("选项B")
                    || !row.getCell(4).getStringCellValue().equals("选项C") || !row.getCell(5).getStringCellValue().equals("选项D")) {
                return false;
            }

            //getLastRowNum()得到Excel的行数,从零开始的
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                if (!judgeSingleChoiceAnswerIfOneToFour(Integer.toString((int) sheet.getRow(i).getCell(1).getNumericCellValue()))) {
                    System.out.println("得到的值:" + sheet.getRow(i).getCell(1).getNumericCellValue() + "  " + (Integer.toString((int) sheet.getRow(i).getCell(1).getNumericCellValue())));
                    return false;
                }
            }
        } else if (topicId == TopicId.MultipleChoiceQuestionExcel) {
            Row row = sheet.getRow(0);
            //getCell获取单元格,getStringCellValue获取单元格中的内容
            if (!row.getCell(0).getStringCellValue().equals("题目") || !row.getCell(1).getStringCellValue().equals("正确选项(正确为小写t,错误为小写f)")
                    || !row.getCell(2).getStringCellValue().equals("选项A") || !row.getCell(3).getStringCellValue().equals("选项B")
                    || !row.getCell(4).getStringCellValue().equals("选项C") || !row.getCell(5).getStringCellValue().equals("选项D")) {
                return false;
            }

            //getLastRowNum()得到Excel的行数,从零开始的
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                if (!judgeMultipleChoiceQuestionAnswerIfUpToStandard(sheet.getRow(i).getCell(1).getStringCellValue())) {
                    return false;
                }
            }
        } else if (topicId == TopicId.JudgeQuestionExcel) {
            Row row = sheet.getRow(0);
            //getCell获取单元格,getStringCellValue获取单元格中的内容
            if (!row.getCell(0).getStringCellValue().equals("题目") || !row.getCell(1).getStringCellValue().equals("正确选项(正确为小写t,错误为小写f)")) {
                return false;
            }

            //getLastRowNum()得到Excel的行数,从零开始的
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                if ((findCharNum(sheet.getRow(i).getCell(1).getStringCellValue(), 't') + findCharNum(sheet.getRow(i).getCell(1).getStringCellValue(), 'f')) != 1) {
                    return false;
                }
            }
        } else if (topicId == TopicId.GapFillingExcel) {
            Row row = sheet.getRow(0);

            int countCell = -1;
            while (row.getCell(++countCell) != null) ;
            //getCell获取单元格,getStringCellValue获取单元格中的内容
            if (!row.getCell(0).getStringCellValue().equals("题目（填空的地方请用三个英文状态的下划线“___”）") || countCell != 1) {
                System.out.println("总数:" + countCell);
                return false;
            }
        } else if (topicId == TopicId.EssayQuestionExcel) {
            Row row = sheet.getRow(0);

            int countCell = -1;
            while (row.getCell(++countCell) != null) ;
            //getCell获取单元格,getStringCellValue获取单元格中的内容
            if (!row.getCell(0).getStringCellValue().equals("题目") || countCell != 1) {
                return false;
            }
        }

        fileInputStream.close();

        return true;
    }

    //把1转成tfff,2转成
    static public String numberTransitionChar(String str) {
        switch (str) {
            case "1":
                return "tfff";
            case "2":
                return "ftff";
            case "3":
                return "fftf";
            case "4":
                return "ffft";
        }
        return "";
    }

    static public boolean judgeSingleChoiceAnswerIfOneToFour(String num) {
        return num.equals("1") || num.equals("2") || num.equals("3") || num.equals("4");
    }

    static public boolean judgeMultipleChoiceQuestionAnswerIfUpToStandard(String str) {
        if (str.length() != 4) {
            return false;
        }

        return (findCharNum(str, 't') + findCharNum(str, 'f')) == 4;
    }

    //查询字符串中某个字符出现的次数
    static public int findCharNum(String str, char c) {
        str = str.toLowerCase();
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == c) {
                count++;
            }
        }
        return count;
    }

    static public int excelNum(String filepath) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(filepath);

        Workbook workbook = new XSSFWorkbook(fileInputStream);

        //得到工作表,第一页
        Sheet sheet = workbook.getSheetAt(0);

        return sheet.getLastRowNum();
    }
}
