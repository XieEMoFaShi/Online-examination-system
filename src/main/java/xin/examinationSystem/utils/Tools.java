package xin.examinationSystem.utils;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Tools {
    static private final ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    static public Object getImp(String str) {
        return context.getBean(str);
    }
}
