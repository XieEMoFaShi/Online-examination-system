package xin.examinationSystem.dao;

import org.apache.ibatis.annotations.Mapper;
import xin.examinationSystem.account.Student;

/**
 * 学生信息操作类,通过mybatis获取实现类
 */
@Mapper
public interface IStudentMapper {

    /**
     * 用于判断学生账户是否存在
     * @param account 学生账号
     * @return 如果查询到返回对应学生账户的实体类,如果返回值为:null说明没有找到
     */
    Student findAccount(String account);

    /**
     * 查询学生id是否存在
     * @param account
     * @return
     */
    Student findStuId(Integer stuId);
}
